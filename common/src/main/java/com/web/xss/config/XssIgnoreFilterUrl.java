package com.web.xss.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

/**
 * xss忽略过滤地址
 *
 * @author LiRui
 * @version 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "xss.ignore")
public class XssIgnoreFilterUrl {
    private Set<String> urls;

    public Set<String> getUrls() {
        return urls;
    }

    public void setUrls(Set<String> urls) {
        this.urls = urls;
    }
}
