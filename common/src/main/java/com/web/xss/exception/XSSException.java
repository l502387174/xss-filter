package com.web.xss.exception;

/**
 * xss异常
 *
 * @author LiRui
 * @version 1.0
 */
public class XSSException extends RuntimeException {
    private String msg;
    private int code = 500;

    public XSSException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
