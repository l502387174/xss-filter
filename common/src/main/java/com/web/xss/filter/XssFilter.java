/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.web.xss.filter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Set;

/**
 * XSS过滤
 *
 * @author lirui
 */
public class XssFilter implements Filter {

    private Set<String> excludedUris;

    public XssFilter() {
    }

    public XssFilter(Set<String> excludedUris) {
        this.excludedUris = excludedUris;
    }

    /**
     * 是否排除
     *
     * @param uri
     * @return
     */
    private boolean isExcludedUri(String uri) {
        if (CollectionUtils.isEmpty(excludedUris)) {
            return false;
        }
        for (String ex : excludedUris) {
            if (match(ex, uri)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 地址匹配
     *
     * @param patternPath
     * @param requestPath
     * @return
     */
    public static boolean match(String patternPath, String requestPath) {
        if (StringUtils.isEmpty(patternPath) || StringUtils.isEmpty(requestPath)) {
            return false;
        }
        PathMatcher matcher = new AntPathMatcher();
        return matcher.match(patternPath, requestPath);
    }


    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        //可配置多个字符串过滤
        XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper(
                (HttpServletRequest) request, new HTMLFilter());
        String url = xssRequest.getServletPath();
        if (isExcludedUri(url)) {
            chain.doFilter(request, response);
            return;
        }
        chain.doFilter(xssRequest, response);
    }

    @Override
    public void destroy() {
    }

}