package com.web.xss.filter;

/**
 * 字符过滤器
 *
 * @author LiRui
 * @version 1.0
 */
public interface CharacterFilter {

    String filter(String input);
}
