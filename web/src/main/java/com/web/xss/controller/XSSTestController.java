package com.web.xss.controller;

import org.springframework.web.bind.annotation.*;

/**
 * 测试类
 *
 * @author LiRui
 * @version 1.0
 */
@RequestMapping("/xss")
@RestController
public class XSSTestController {


    @GetMapping("/form")
    public Object xssFormTest(String value) {
        return value;
    }

    @PostMapping("/json")
    public Object xssJsonTest(@RequestBody String value) {
        return value;
    }
}
